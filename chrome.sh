#!/bin/bash

if [ "$(whoami)" != "root" ]; then
  read -p "This script helps install Chrome dependencies using APT installers. You'll be prompted for sudo access, so please look at the contents. (enter to continue)";
  su root;
fi


if [ ! -f "/root/.cache/secret-agent/chrome/88.0.4324.182/.validated" ]; then
  chown _apt /root/.cache/secret-agent/chrome/88.0.4324.182/install-dependencies.deb;
  apt-get update;
  apt install -y /root/.cache/secret-agent/chrome/88.0.4324.182/install-dependencies.deb;
  apt-get -y autoremove;
  touch /root/.cache/secret-agent/chrome/88.0.4324.182/.validated;
  chmod 777 /root/.cache/secret-agent/chrome/88.0.4324.182/.validated;
fi
