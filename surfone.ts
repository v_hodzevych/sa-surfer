import agent from 'secret-agent';

(async () => {
    const href: string = 'https://www.joom.com/uk'
    await agent.goto(href)
    const title = await agent.document.title;
    agent.output = { title };
    await agent.close();

    console.log(`Retrieved from ${href}`, title);
})();