"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const secret_agent_1 = require("secret-agent");
(() => __awaiter(void 0, void 0, void 0, function* () {
    const href = 'https://www.joom.com/uk';
    yield secret_agent_1.default.goto(href);
    const title = yield secret_agent_1.default.document.title;
    secret_agent_1.default.output = { title };
    yield secret_agent_1.default.close();
    console.log(`Retrieved from ${href}`, title);
}))();
//# sourceMappingURL=main.js.map