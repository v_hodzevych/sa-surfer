import agent from 'secret-agent';

const hrefs= [
    "https://kasta.ua/",
    "https://leboutique.com/",
    "https://www.myntra.com/",
    "https://www.next.ua/en",
    "https://slotscity.ua/ru",
    "https://first.ua/",
    "https://parimatch.com/ru/casino/slots"
]
const maxTotalTime = 5*60000 //time in milliseconds

surfer(hrefs)

async function surfer(hrefs){
    const n = hrefs.length
    for (let i=0;i<n;i++)
        await surfOneSite(hrefs[i], maxTotalTime/n)
    await agent.close()
}

async function surfOneSite(href, timeBudget) {
    const start = Date.now()
    console.log(href)
    try {
        await agent.goto(href)
        const title = await agent.document.title
        console.log(title)
        for (let y = 10; (Date.now()  - start) < timeBudget; y += randomNaturalLimited(50)){
            console.log(Date.now())
            sleepRandomLimited()
            await agent.interact({scroll: [70, y]})
        }
    }
    finally {}
}

async function sleepRandomLimited(max=100){
    await new Promise(resolve => setTimeout(resolve, randomNaturalLimited()))
}

function randomNaturalLimited(max){
    return 1+Math.floor(Math.random() * max )
}